﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ApelsinkaBot.DAL.Migrations
{
    public partial class histories_fixes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SurveyHistories_survey_points_survey_point_id",
                table: "SurveyHistories");

            migrationBuilder.DropPrimaryKey(
                name: "PK_SurveyHistories",
                table: "SurveyHistories");

            migrationBuilder.RenameTable(
                name: "SurveyHistories",
                newName: "survey_histories");

            migrationBuilder.RenameIndex(
                name: "IX_SurveyHistories_survey_point_id",
                table: "survey_histories",
                newName: "IX_survey_histories_survey_point_id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_survey_histories",
                table: "survey_histories",
                column: "id");

            migrationBuilder.AddForeignKey(
                name: "FK_survey_histories_survey_points_survey_point_id",
                table: "survey_histories",
                column: "survey_point_id",
                principalTable: "survey_points",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_survey_histories_survey_points_survey_point_id",
                table: "survey_histories");

            migrationBuilder.DropPrimaryKey(
                name: "PK_survey_histories",
                table: "survey_histories");

            migrationBuilder.RenameTable(
                name: "survey_histories",
                newName: "SurveyHistories");

            migrationBuilder.RenameIndex(
                name: "IX_survey_histories_survey_point_id",
                table: "SurveyHistories",
                newName: "IX_SurveyHistories_survey_point_id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_SurveyHistories",
                table: "SurveyHistories",
                column: "id");

            migrationBuilder.AddForeignKey(
                name: "FK_SurveyHistories_survey_points_survey_point_id",
                table: "SurveyHistories",
                column: "survey_point_id",
                principalTable: "survey_points",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
