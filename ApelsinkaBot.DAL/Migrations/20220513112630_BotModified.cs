﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ApelsinkaBot.DAL.Migrations
{
    public partial class BotModified : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "name",
                table: "bots",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "telegram_username",
                table: "bots",
                type: "text",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "name",
                table: "bots");

            migrationBuilder.DropColumn(
                name: "telegram_username",
                table: "bots");
        }
    }
}
