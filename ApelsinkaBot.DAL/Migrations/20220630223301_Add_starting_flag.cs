﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ApelsinkaBot.DAL.Migrations
{
    public partial class Add_starting_flag : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "type",
                table: "survey_points",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "is_started",
                table: "bots",
                type: "boolean",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "type",
                table: "survey_points");

            migrationBuilder.DropColumn(
                name: "is_started",
                table: "bots");
        }
    }
}
