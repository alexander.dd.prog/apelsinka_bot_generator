﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ApelsinkaBot.DAL.Migrations
{
    public partial class TelegramId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "telegram_id",
                table: "users",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "telegram_id",
                table: "users");
        }
    }
}
