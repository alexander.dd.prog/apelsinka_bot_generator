﻿using ApelsinkaBot.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using ApelsinkaBot.DAL.Repositories.Contracts;

namespace ApelsinkaBot.DAL.Repositories
{
    public class NotificationRepository : BaseRepositoryWithId<Notification>, INotificationRepository
    {
        public override DbSet<Notification> dbTable => db.Notifications;
    }
}
