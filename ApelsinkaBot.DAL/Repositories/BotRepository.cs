﻿using ApelsinkaBot.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using ApelsinkaBot.DAL.Repositories.Contracts;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;

namespace ApelsinkaBot.DAL.Repositories
{
    public class BotRepository : BaseRepositoryWithId<BotInfo>, IBotRepository
    {
        public override DbSet<BotInfo> dbTable => db.Bots;

        public async Task<List<BotInfo>> GetAllUserBots(int userId)
        {
            return dbTable.Where(sc => sc.UserId == userId).ToList<BotInfo>();
        }
    }
}
