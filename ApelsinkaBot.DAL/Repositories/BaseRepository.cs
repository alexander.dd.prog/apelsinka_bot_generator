﻿using ApelsinkaBot.DAL.Context;
using ApelsinkaBot.DAL.Repositories.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApelsinkaBot.DAL.Repositories
{
    /// <summary>
    /// Методы в этом классе не менять. Логика в наследниках.
    /// </summary>
    public abstract class BaseRepository<T> : IBaseRepository<T> where T : class
    {
        protected ApelsinkaDBContext db = new ApelsinkaDBContext();
        public abstract DbSet<T> dbTable { get; }

        public virtual Task<List<T>> GetAll()
        {
            return dbTable.ToListAsync();
        }

        public virtual Task Delete(T entity)
        {
            return Task.Run(() => dbTable.Remove(entity));
        }

        public virtual Task Insert(T entity)
        {
            return dbTable.AddAsync(entity).AsTask();
        }

        public virtual Task Update(T entity)
        {
            return Task.Run(() => dbTable.Update(entity));
        }

        public virtual Task SaveChanges()
        {
            return db.SaveChangesAsync();
        }
    }
}
