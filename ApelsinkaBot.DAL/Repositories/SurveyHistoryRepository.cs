﻿using ApelsinkaBot.DAL.Entities;
using ApelsinkaBot.DAL.Repositories.Contracts;
using Microsoft.EntityFrameworkCore;

namespace ApelsinkaBot.DAL.Repositories
{
    public class SurveyHistoryRepository : BaseRepositoryWithId<SurveyHistory>, ISurveyHistoryRepository
    {
        public override DbSet<SurveyHistory> dbTable => db.SurveyHistories;
    }
}
