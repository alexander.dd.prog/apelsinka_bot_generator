﻿using ApelsinkaBot.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using ApelsinkaBot.DAL.Repositories.Contracts;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

namespace ApelsinkaBot.DAL.Repositories
{
    public class SurveyConnectionRepository : BaseRepository<SurveyConnection>, ISurveyConnectionRepository
    {
        public override DbSet<SurveyConnection> dbTable => db.SurveyConnections;

        public Task<List<int>> GetByParentID(int parentID)
        {
            return Task.Run(() => dbTable.Where(sc => sc.ParentSurveyPointId == parentID)
                .Select(s => s.SurveyPointId)
                .ToList());
        }
    }
}
