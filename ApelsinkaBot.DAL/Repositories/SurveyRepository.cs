﻿using ApelsinkaBot.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using ApelsinkaBot.DAL.Repositories.Contracts;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;

namespace ApelsinkaBot.DAL.Repositories
{
    public class SurveyRepository : BaseRepositoryWithId<Survey>, ISurveyRepository
    {
        public override DbSet<Survey> dbTable => db.Surveys;

        public Task<List<Survey>> GetAllBotSurveys(int bot_id)
        {
            return Task.Run(() => dbTable.Where(survey => survey.BotId == bot_id).ToList());
        }
    }
}
