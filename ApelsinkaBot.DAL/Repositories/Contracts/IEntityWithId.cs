﻿namespace ApelsinkaBot.DAL.Repositories.Contracts
{
    public interface IEntityWithId
    {
        int Id { get; set; }
    }
}
