﻿using ApelsinkaBot.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApelsinkaBot.DAL.Repositories.Contracts
{
    public interface ISurveyRepository : IBaseRepository<Survey>, IRepositoryWithId<Survey>
    {
        Task<List<Survey>> GetAllBotSurveys(int bot_id);
    }
}
