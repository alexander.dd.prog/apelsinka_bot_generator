﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace ApelsinkaBot.DAL.Repositories.Contracts
{
    public interface IRepositoryWithId<T> where T : class
    {
        Task<T> GetBySingleId(int id);
        Task<List<T>> GetByMultipleIds(IEnumerable<int> id);
    }
}
