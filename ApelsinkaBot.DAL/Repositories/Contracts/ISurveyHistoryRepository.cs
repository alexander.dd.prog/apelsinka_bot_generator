﻿using ApelsinkaBot.DAL.Entities;

namespace ApelsinkaBot.DAL.Repositories.Contracts
{
    public interface ISurveyHistoryRepository : IBaseRepository<SurveyHistory>, IRepositoryWithId<SurveyHistory>
    {
    }
}
