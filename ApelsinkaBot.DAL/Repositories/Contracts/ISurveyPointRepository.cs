﻿using ApelsinkaBot.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApelsinkaBot.DAL.Repositories.Contracts
{
    public interface ISurveyPointRepository : IBaseRepository<SurveyPoint>, IRepositoryWithId<SurveyPoint>
    {
    }
}
