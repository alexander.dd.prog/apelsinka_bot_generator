﻿using ApelsinkaBot.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApelsinkaBot.DAL.Repositories.Contracts
{
    public interface IUserRepository: IBaseRepository<User>, IRepositoryWithId<User>
    {
        Task<User> GetUserByPhone(string phone);
        Task<User> GetUserByTelegramId(long telegramId);
    }
}
