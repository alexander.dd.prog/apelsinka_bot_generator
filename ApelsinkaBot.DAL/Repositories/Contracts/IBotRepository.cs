﻿using ApelsinkaBot.DAL.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ApelsinkaBot.DAL.Repositories.Contracts
{
    public interface IBotRepository : IBaseRepository<BotInfo>, IRepositoryWithId<BotInfo>
    {
        Task<List<BotInfo>> GetAllUserBots(int userId);
    }
}
