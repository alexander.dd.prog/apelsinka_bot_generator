﻿using ApelsinkaBot.DAL.Repositories.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace ApelsinkaBot.DAL.Repositories
{
    public abstract class BaseRepositoryWithId<T> : BaseRepository<T>, IRepositoryWithId<T>where T : class, IEntityWithId
    {
        public virtual Task<T> GetBySingleId(int id)
        {
            return dbTable.FindAsync(id).AsTask();
        }

        public virtual Task<List<T>> GetByMultipleIds(IEnumerable<int> id)
        {
            return Task.Run(() => dbTable.Where(record => id.Contains(record.Id)).ToList());
        }
    }
}
