﻿using ApelsinkaBot.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using ApelsinkaBot.DAL.Repositories.Contracts;
using System.Threading.Tasks;
using System.Linq;

namespace ApelsinkaBot.DAL.Repositories
{
    public class UserRepository : BaseRepositoryWithId<User>, IUserRepository
    {
        public override DbSet<User> dbTable => db.Users;

        public async Task<User> GetUserByPhone(string phone) 
        {
            return await Task.Run(() => dbTable.Where(sc => sc.PhoneNumber == phone).FirstOrDefault());
        }
        public async Task<User> GetUserByTelegramId(long telegramId)
        {
            return await Task.Run(() => dbTable.Where(sc => sc.TelegramId == telegramId).FirstOrDefault());
        }
    }
}
