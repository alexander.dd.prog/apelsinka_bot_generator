﻿using ApelsinkaBot.DAL.Entities;
using ApelsinkaBot.DAL.Repositories.Contracts;
using Microsoft.EntityFrameworkCore;

namespace ApelsinkaBot.DAL.Repositories
{
    public class SurveyPointRepository : BaseRepositoryWithId<SurveyPoint>, ISurveyPointRepository
    {
        public override DbSet<SurveyPoint> dbTable => db.SurveyPoints;
    }
}
