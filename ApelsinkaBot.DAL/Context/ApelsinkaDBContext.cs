﻿using Microsoft.EntityFrameworkCore;
using ApelsinkaBot.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ApelsinkaBot.DAL.Properties;

namespace ApelsinkaBot.DAL.Context
{
    public class ApelsinkaDBContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<BotInfo> Bots { get; set; }
        public DbSet<SurveyPoint> SurveyPoints { get; set; }
        public DbSet<Survey> Surveys { get; set; }
        public DbSet<Service> Services { get; set; }
        public DbSet<Notification> Notifications { get; set; }
        public DbSet<SurveyConnection> SurveyConnections { get; set; }
        public DbSet<SurveyHistory> SurveyHistories { get; set; }

        public ApelsinkaDBContext()
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseNpgsql(Resources.ConnectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<SurveyConnection>(en => en.HasNoKey());
            builder.Entity<SurveyConnection>().ToTable("survey_connection");
            builder.Entity<User>().ToTable("users");
            builder.Entity<BotInfo>().ToTable("bots");
            builder.Entity<SurveyPoint>().ToTable("survey_points");
            builder.Entity<Survey>().ToTable("surveys");
            builder.Entity<Service>().ToTable("services");
            builder.Entity<Notification>().ToTable("notifications");
        }
    }
}
