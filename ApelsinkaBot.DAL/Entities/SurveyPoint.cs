﻿using ApelsinkaBot.DAL.Repositories.Contracts;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApelsinkaBot.DAL.Entities
{
    /// <summary>
    /// Конкретный вопрос в опросе, может быть соединён с другим вопросом, с помощью SurveyConnection.
    /// </summary>

    public class SurveyPoint : IEntityWithId
    {
        [Column("id")]
        public int Id { get; set; }

        [Column("survey_text")]
        public string SurveyText { get; set; }

        [Column("type")]
        public string Type { get; set; }
    }
}
