﻿using ApelsinkaBot.DAL.Repositories.Contracts;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApelsinkaBot.DAL.Entities
{
    public class Service : IEntityWithId
    {
        [Column("id")]
        public int Id { get; set; }

        [Column("name")]
        public string Name { get; set; }
    }
}
