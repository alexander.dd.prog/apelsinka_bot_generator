﻿using ApelsinkaBot.DAL.Repositories.Contracts;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApelsinkaBot.DAL.Entities
{
    public class Notification : IEntityWithId
    {
        [Column("id")]
        public int Id { get; set; }

        [Column("name")]
        public string Name { get; set; }

        public BotInfo Bot { get; set; }
        [Column("bot_id")]
        public int BotId { get; set; }

        [Column("frequency")]
        public uint Frequency { get; set; }

        public Service Service { get; set; }
        [Column("service_id")]
        public int ServiceId { get; set; }
    }
}
