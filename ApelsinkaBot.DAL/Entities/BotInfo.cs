﻿using ApelsinkaBot.DAL.Repositories.Contracts;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApelsinkaBot.DAL.Entities
{
    [Table("bots")]
    public class BotInfo : IEntityWithId
    {
        [Column("id")]
        public int Id { get; set; }

        [Column("token")]
        public string Token { get; set; }

        [Column("name")]
        public string Name { get; set; }

        [Column("telegram_username")]
        public string TelegramUsername { get; set; }

        public User User { get; set; }

        [Column("user_id")]
        public int UserId { get; set; }

        [Column("is_started")]
        public bool IsStarted { get; set; }

    }
}
