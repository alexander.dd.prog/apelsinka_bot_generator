﻿using ApelsinkaBot.DAL.Repositories.Contracts;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApelsinkaBot.DAL.Entities
{
    public class User : IEntityWithId
    {
        [Column("id")]
        public int Id { get; set; }

        [Column("phone_number")]
        public string PhoneNumber { get; set; }

        [Column("telegram_id")]
        public long TelegramId { get; set; }
    }
}
