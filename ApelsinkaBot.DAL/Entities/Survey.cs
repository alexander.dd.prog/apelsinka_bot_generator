﻿using ApelsinkaBot.DAL.Repositories.Contracts;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApelsinkaBot.DAL.Entities
{
    /// <summary>
    /// Опрос, привязывается к боту.
    /// Включает в себя ссылку на первый элемент дерева, состоящего из вопросов (SurveyPoint) и соединений (SurveyConnection).
    /// </summary>
    public class Survey: IEntityWithId
    {
        [Column("id")]
        public int Id { get; set; }

        [Column("name")]
        public string Name { get; set; }

        public BotInfo Bot { get; set; }
        [Column("bot_id")]
        public int BotId { get; set; }

        public SurveyPoint FirstSurveyPoint { get; set; }
        [Column("first_point_id")]
        public int FirstSurveyPointId { get; set; }
    }
}
