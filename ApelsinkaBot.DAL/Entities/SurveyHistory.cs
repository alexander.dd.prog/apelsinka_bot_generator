﻿using ApelsinkaBot.DAL.Repositories.Contracts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApelsinkaBot.DAL.Entities
{
    /// <summary>
    /// Записи прохождения опроса пользователем в UserBot.
    /// </summary>
    [Table("survey_histories")]
    public class SurveyHistory : IEntityWithId
    {
        [Column("id")]
        public int Id { get; set; }

        [Column("chat_id")]
        public long ChatId { get; set; }

        [Column("dt")]
        public DateTime Dt { get; set; }

        public SurveyPoint SurveyPoint { get; set; }

        [Column("survey_point_id")]
        public int SurveyPointId { get; set; }


    }
}
