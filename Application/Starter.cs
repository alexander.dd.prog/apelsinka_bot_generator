﻿using ApelsinkaBot.Application.Properties;
using ApelsinkaBot.BLL.Bots;
using ApelsinkaBot.BLL.Services.Contracts;
using ApelsinkaBot.DAL.Entities;
using ApelsinkaBot.RabbitMQ;

namespace ApelsinkaBot.Application
{
    internal class Starter
    {
        private readonly IUserService _userService;
        private readonly IBotService _botService;

        protected readonly ISurveyService _surveyService;
        protected readonly ISurveyPointService _surveyPointService;
        protected readonly ISurveyHistoryService _surveyHistoryService;

        private Executor _executor = new Executor();
        private RabbitConsumer _rabbitConsumer = new RabbitConsumer();

        public Starter(IUserService userService, IBotService botService, ISurveyService surveyService, ISurveyPointService surveyPointService, IServiceProvider serviceProvider, ISurveyHistoryService surveyHistoryService)
        {
            _userService = userService;
            _botService = botService;

            _surveyService = surveyService;
            _surveyPointService = surveyPointService;
            _surveyHistoryService = surveyHistoryService;
        }

        public async Task StartAsync()
        {
            // ::::::::::::::::::::::: MainBOT :::::::::::::::::::::::
            MainBot mainBot = new MainBot(new BotInfo() { Name = "MainBot", Token = Resources.ResourceManager.GetString("MainBotToken") }, _userService, _botService);
            mainBot.UserBotStart = botInfo => _executor.TryStart(new UserBot(botInfo, _surveyService, _surveyPointService, _surveyHistoryService)).Result;
            mainBot.OnUserBotStop = bot => _executor.Stop(bot);

            var mainBotTask = _executor.TryStart(mainBot);

            // ::::::::::::::::::::::: RabbitMQ :::::::::::::::::::::::
            CurrencyBot currencyBot = new CurrencyBot(new BotInfo() { Name = "CurrencyBot", Token = Resources.ResourceManager.GetString("CurrencyBotToken") });
            _rabbitConsumer.OnMessageReceived = currencyBot.OnRabbitMessageHandler;

            await _executor.TryStart(currencyBot);
            _rabbitConsumer.Start();
            
            await mainBotTask;
        }

        public async Task StopAsync()
        {
            _rabbitConsumer.Stop();
            await _executor.StopAll();
        }
    }
}
