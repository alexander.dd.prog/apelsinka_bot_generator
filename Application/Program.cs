﻿using ApelsinkaBot.BLL.Configuration;
using ApelsinkaBot.DAL.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ApelsinkaBot.Application
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            //setup our DI
            IServiceCollection services = new ServiceCollection();

            services.ConfigureServices();
            services.ConfigureRepositories();

            services.AddSingleton<Starter>();

            ServiceProvider serviceProvider = services.BuildServiceProvider();

            //do the actual work here
            Starter? starter = serviceProvider.GetService<Starter>();

            if (starter is not null)
            {
                await starter.StartAsync();
                Console.WriteLine("All started");
                Console.ReadLine(); // TODO: Избавиться от этой конструкции.
                //starter.Stop();
            }
        }
    }
}