﻿using ApelsinkaBot.BLL.Bots;

namespace ApelsinkaBot.Application
{
    public class Executor
    {
        public List<IBot> Bots { get; set; } = new List<IBot>();

        public async Task StopAll()
        {
            foreach (IBot bot in Bots)
            {
                await Stop(bot);
            }
        }

        // TODO:
        //public void AddBot(IBot bot)
        //{
        //    Bots.Add(bot);
        //}

        public void RemoveBot(IBot bot)
        {
            Bots.Remove(bot);
        }

        public async Task<bool> TryStart(IBot bot)
        {
            if (!Bots.Any(b => b.BotInfo.Token == bot.BotInfo.Token))
            {
                if (await bot.StartAsync())
                {
                    Console.WriteLine($"Bot {bot.BotInfo.Name} started!");
                    Bots.Add(bot);
                    return true;
                }
            }

            return false;
        }

        public Task Stop(IBot bot)
        {
            return Task.Run(() =>
            {
                bot.Stop();
                Bots.Remove(bot);
            });
        }
    }
}
