﻿namespace ApelsinkaBot.RabbitMQ
{
    public class Program
    {
        public static void Main(string[] args)
        {
            RatesProducer _ratesProducer = new RatesProducer();
            _ratesProducer.Start();
            Console.ReadLine();

        }
    }
}