﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Text;

namespace ApelsinkaBot.RabbitMQ
{
    public class RabbitConsumer : RabbitBase
    {
        private const string 
            connectionString = "localhost",
            queue = "ApelsinkaNotifications",
            exchange = "TelegramNotifications",
            routingKey = "ExchangeRates";

        public Action<string, string> OnMessageReceived { get; set; }

        public IModel _channel { get; set; }
        private EventingBasicConsumer _consumer { get; set; }
        public RabbitConsumer() : base(connectionString)
        {
            if (Connection is not null && Connection.IsOpen)
            {
                Console.WriteLine("RabbitConsumer initialyzed!");
            }
        }

        public void Start()
        {
            _channel = Connection.CreateModel();

            _channel.QueueBind(queue: queue, exchange: exchange, routingKey: routingKey);
            _channel.BasicQos(0, 1, false);

            _consumer = new EventingBasicConsumer(_channel);
            _consumer.Received += (model, ea) =>
            {
                var body = ea.Body.ToArray();
                var message = Encoding.UTF8.GetString(body);
                //Console.WriteLine(" [x] Received {0}", message);
                Console.WriteLine(" [x] Receive rates");
                OnMessageReceived(ea.RoutingKey, message);
            };

            _channel.BasicConsume(queue: queue,
                            autoAck: true,
                            consumer: _consumer);
        }

        public void Stop()
        {
            //_channel.Close();

        }
    }
}
