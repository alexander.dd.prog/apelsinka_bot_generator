﻿using RabbitMQ.Client;

namespace ApelsinkaBot.RabbitMQ
{
    public class RabbitBase
    {
        protected IConnection Connection;
        public RabbitBase(string hostName)
        {
            var factory = new ConnectionFactory() { HostName = hostName };
            Connection = factory.CreateConnection();
        }
    }
}
