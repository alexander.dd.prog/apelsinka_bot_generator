﻿using ApelsinkaBot.RabbitMQ;
using ApelsinkaBot.RabbitMQ.Properties;
using Newtonsoft.Json;
using RatesExchangeApi;

namespace ApelsinkaBot.RabbitMQ
{
    public class RatesProducer
    {
        private const string BaseCurrency = "RUB";
        private static readonly List<string> RatesCurrencies = new List<string> { "USD", "EUR", "JPY" };
        private double TimerInterval = 10 * 1000;
        private string RabbitConnectionString = "localhost";
        private record RatesOnDate(Dictionary<string, decimal> rates, DateTime date);
        private DateTime CurrentDate { get; set; }
        System.Timers.Timer Timer { get; set; }
        RabbitSender Sender { get; set; }

        public RatesProducer()
        {
            Timer = new System.Timers.Timer(TimerInterval);
            CurrentDate = new DateTime(2021, 3, 1);
            Sender = new RabbitSender(RabbitConnectionString);
        }

        public void Start()
        {
            Timer.Elapsed += SendRatesToRabbitMQ;
            Timer.Start();
        }

        public void Stop()
        {
            Timer.Elapsed -= SendRatesToRabbitMQ;
            Timer.Stop();
        }

        private void SendRatesToRabbitMQ(object? sender, System.Timers.ElapsedEventArgs e)
        { 
            Dictionary<string, decimal> rates = new Dictionary<string, decimal>();
            foreach (string currency in RatesCurrencies)
            {
                var rate = GetRate(CurrentDate, currency);
                if (rate != 0)
                {
                    rates[currency] = rate;
                } 
            }; 

            var data = new RatesOnDate(rates, CurrentDate);
            var parsedData = JsonConvert.SerializeObject(data, Formatting.Indented);

            Sender.SendMessageAsync("TelegramNotifications", "ExchangeRates", parsedData).Wait();

            CurrentDate = CurrentDate.AddDays(1);
        }

        public static decimal GetRate(DateTime Date, string Currency)
        {
            var client = new RatesExchangeApiService(Resources.ResourceManager.GetString("ApiKey"));
            try
            {
                var rates = client.GetHistoryRates(Currency, Date.ToString("yyyy-MM-dd"), new List<string> { BaseCurrency }).Result;
                if (rates.Rates.ContainsKey(BaseCurrency)) return rates.Rates[BaseCurrency];
                else return 0;
            }
            catch
            { 
                return 0; 
            }
        }
    }
}
