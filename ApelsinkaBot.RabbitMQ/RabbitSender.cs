﻿using RabbitMQ.Client;
using System.Text;

namespace ApelsinkaBot.RabbitMQ
{
    public class RabbitSender : RabbitBase
    {
        public RabbitSender(string hostName) : base(hostName)
        {
            if (Connection is not null && Connection.IsOpen)
            {
                Console.WriteLine("RabbitSender initialyzed!");
            }
        }

        public Task SendMessageAsync(string exchange, string routingKey, string message)
        {
            return Task.Run(() =>
            {
                using (var channel = Connection.CreateModel())
                {
                    channel.BasicPublish(exchange: exchange,
                                        routingKey: routingKey,
                                        basicProperties: null,
                                        body: Encoding.UTF8.GetBytes(message));
                };
            });
        }
    }
}
