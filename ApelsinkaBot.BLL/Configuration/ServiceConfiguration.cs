﻿using ApelsinkaBot.BLL.Services;
using ApelsinkaBot.BLL.Services.Contracts;
using Microsoft.Extensions.DependencyInjection;

namespace ApelsinkaBot.BLL.Configuration
{
    public static class ServiceConfiguration
    {
        public static void ConfigureServices(this IServiceCollection services)
        {
            services.AddScoped<IBotService, BotService>();
            services.AddScoped<INotificationService, NotificationService>();

            services.AddScoped<ISurveyService, SurveyService>();
            services.AddScoped<ISurveyPointService, SurveyPointService>();
            services.AddScoped<ISurveyHistoryService, SurveyHistoryService>();

            services.AddScoped<IUserService, UserService>();
        }
    }
}
