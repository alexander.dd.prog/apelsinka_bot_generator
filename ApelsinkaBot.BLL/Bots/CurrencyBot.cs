﻿using ApelsinkaBot.DAL.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;

namespace ApelsinkaBot.BLL.Bots
{
    public class CurrencyBot : BotBase
    {
        private static readonly List<string> RatesCurrencies = new List<string> { "USD", "EUR", "JPY" };
        private ConcurrentDictionary<Telegram.Bot.Types.ChatId, List<string>> Subscribed { get; set; } = new();
        private record RatesOnDate(Dictionary<string, decimal> rates, DateTime date);

        public CurrencyBot(BotInfo botInfo) : base(botInfo)
        {
            UsageCaption += "/subscribe - subscribe currency" + Environment.NewLine;
            UsageCaption += "/unsubscribe - subscribe currency" + Environment.NewLine;
            UsageCaption += "/unsubscribeall - subscribe currency" + Environment.NewLine;
            // TODO: UsageCaption += "/list - show subscribed list" + Environment.NewLine;
        }

        public void OnRabbitMessageHandler(string routingKey, string text)
        {
            if (routingKey == "ExchangeRates")
            {
                RatesOnDate ratesOnDate = JsonConvert.DeserializeObject<RatesOnDate>(text);

                foreach (KeyValuePair<Telegram.Bot.Types.ChatId, List<string>> subscribe in Subscribed)
                {
                    string message = string.Empty;

                    foreach (string currency in subscribe.Value)
                    {
                        if (ratesOnDate.rates.ContainsKey(currency))
                        {
                            message += $"Currency {currency} on date {ratesOnDate.date.ToString("dd.MM.yyyy")} equals {ratesOnDate.rates[currency]}" + Environment.NewLine;
                        }
                    }

                    Client.SendTextMessageAsync(chatId: subscribe.Key, text: message);
                }
            }
        }

        protected override async Task BotOnMessageReceived(ITelegramBotClient botClient, Message message)
        {
            Console.WriteLine($"Receive in CurrencyBot, message type: {message.Type}");
            if (message.Text is not { } messageText)
                return;

            Message sentMessage = messageText.Split(' ')[0] switch
            {
                "/subscribe" => await Subscribe(botClient, message),
                "/unsubscribe" => await Unsubscribe(botClient, message),
                "/unsubscribeall" => await UnsubscribeAll(botClient, message),
                // TODO: "/list" => await ShowSubscribeList(botClient, message),
                _ => null
            };

            if (sentMessage is null)
            {
                await base.BotOnMessageReceived(botClient, message);
                return;
            }

            Console.WriteLine($"The message was sent with id: {sentMessage.MessageId}");

            async Task<Message> Subscribe(ITelegramBotClient botClient, Message message)
            {
                await botClient.SendChatActionAsync(message.Chat.Id, ChatAction.Typing);

                InlineKeyboardMarkup inlineKeyboard = new(
                    RatesCurrencies.Select(rate => InlineKeyboardButton.WithCallbackData(rate, "/subscribe " + rate))
                );

                return await botClient.SendTextMessageAsync(chatId: message.Chat.Id,
                                                            text: "Choose rate to subscribe:",
                                                            replyMarkup: inlineKeyboard);
            }

            async Task<Message> Unsubscribe(ITelegramBotClient botClient, Message message)
            {
                await botClient.SendChatActionAsync(message.Chat.Id, ChatAction.Typing);

                InlineKeyboardMarkup inlineKeyboard = new(
                    RatesCurrencies.Select(rate => InlineKeyboardButton.WithCallbackData(rate, "/unsubscribe " + rate))
                );

                return await botClient.SendTextMessageAsync(chatId: message.Chat.Id,
                                                            text: "Choose rate to unsubscribe:",
                                                            replyMarkup: inlineKeyboard);
            }

            async Task<Message> UnsubscribeAll(ITelegramBotClient botClient, Message message)
            {
                await botClient.SendChatActionAsync(message.Chat.Id, ChatAction.Typing);

                if (Subscribed.ContainsKey(message.Chat.Id))
                {
                    Subscribed[message.Chat.Id] = new List<string>();
                }

                return await botClient.SendTextMessageAsync(chatId: message.Chat.Id,
                                                            text: $"You unsubscribed from all!");
            }
        }

        protected override async Task BotOnCallbackQueryReceived(ITelegramBotClient botClient, CallbackQuery callbackQuery)
        {
            if (callbackQuery.Data is not { } dataText)
                return;

            string[] split = dataText.Split(' ');

            if (split.Length < 2)
            {
                await botClient.SendTextMessageAsync(chatId: callbackQuery.Message.Chat.Id,
                                            text: "Command is not defined");
                return;
            }

            var action = split[0] switch
            {
                "/subscribe" => Subscribe(botClient, callbackQuery, split[1]),
                "/unsubscribe" => Unsubscribe(botClient, callbackQuery, split[1]),
                // TODO: "/stop" => Stop(botClient, callbackQuery),

                _ => null
            };

            async Task<Message> Subscribe(ITelegramBotClient botClient, CallbackQuery callbackQuery, string currency)
            {
                await botClient.SendChatActionAsync(callbackQuery.Message.Chat.Id, ChatAction.Typing);

                if (!Subscribed.ContainsKey(callbackQuery.Message.Chat.Id))
                {
                    Subscribed[callbackQuery.Message.Chat.Id] = new List<string>();
                }

                if (Subscribed[callbackQuery.Message.Chat.Id].Contains(currency))
                {
                    return await botClient.SendTextMessageAsync(chatId: callbackQuery.Message.Chat.Id,
                                                                text: $"You already have this currency: {currency}");
                }

                Subscribed[callbackQuery.Message.Chat.Id].Add(currency);

                return await botClient.SendTextMessageAsync(chatId: callbackQuery.Message.Chat.Id,
                                                            text: $"You subscribed to {currency}!");
            }

            async Task<Message> Unsubscribe(ITelegramBotClient botClient, CallbackQuery callbackQuery, string currency)
            {
                await botClient.SendChatActionAsync(callbackQuery.Message.Chat.Id, ChatAction.Typing);

                if (Subscribed.ContainsKey(callbackQuery.Message.Chat.Id))
                {
                    if (Subscribed[callbackQuery.Message.Chat.Id].Contains(currency))
                    {
                        Subscribed[callbackQuery.Message.Chat.Id].Remove(currency);
                        return await botClient.SendTextMessageAsync(chatId: callbackQuery.Message.Chat.Id,
                                                                    text: $"You unsubscribed from currency: {currency}");
                    }
                }

                return await botClient.SendTextMessageAsync(chatId: callbackQuery.Message.Chat.Id,
                                                            text: $"Not need to unsubscribe {currency}!");
            }
        }
    }
}
