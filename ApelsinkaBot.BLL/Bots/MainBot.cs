﻿using ApelsinkaBot.BLL.Services.Contracts;
using ApelsinkaBot.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;

namespace ApelsinkaBot.BLL.Bots
{
    public class MainBot : BotBase
    {
        public event EventHandler<IBot> OnUserBotAdd;
        public event EventHandler<IBot> OnUserBotRemove;

        public Func<BotInfo, bool> UserBotStart;
        public Action<IBot> OnUserBotStop;

        protected readonly IUserService _userService;
        protected readonly IBotService _botService;

        public MainBot(BotInfo mainBotInfo, IUserService userService, IBotService botService) : base(mainBotInfo)
        {
            _userService = userService;
            _botService = botService;

            UsageCaption += "/bots - bots list" + Environment.NewLine;
        }

        protected override async Task BotOnMessageReceived(ITelegramBotClient botClient, Message message)
        {
            Console.WriteLine($"Receive in MainBot, message type: {message.Type}");
            if (message.Text is not { } messageText)
                return;

            Message sentMessage = messageText.Split(' ')[0] switch
            {
                "/bots" => await ShowUserBots(botClient, message),
                _ => null
            };

            if (sentMessage is null)
            {
                await base.BotOnMessageReceived(botClient, message);
                return;
            }

            Console.WriteLine($"The message was sent with id: {sentMessage.MessageId}");

            async Task<Message> ShowUserBots(ITelegramBotClient botClient, Message message)
            {
                await botClient.SendChatActionAsync(message.Chat.Id, ChatAction.Typing);

                List<BotInfo> allBots = GetAllBotsByUserId(message.Chat.Id).Result;

                if (allBots.Count == 0)
                {
                    return await botClient.SendTextMessageAsync(chatId: message.Chat.Id,
                                                                text: "You have no bots :(\n to add a bot, write /addbot");
                }

                InlineKeyboardMarkup inlineKeyboard = new(
                    allBots.Select(bot => InlineKeyboardButton.WithCallbackData(bot.Name, "/interact " + bot.Name))
                );

                return await botClient.SendTextMessageAsync(chatId: message.Chat.Id,
                                                            text: "Choose bot to interact:",
                                                            replyMarkup: inlineKeyboard);
            }
        }

        protected override async Task BotOnCallbackQueryReceived(ITelegramBotClient botClient, CallbackQuery callbackQuery)
        {
            if (callbackQuery.Data is not { } dataText)
                return;

            string[] split = dataText.Split(' ');

            if (split.Length < 2)
            {
                await botClient.SendTextMessageAsync(chatId: callbackQuery.Message.Chat.Id,
                                            text: "Command is not defined");
                return;
            }

            var action = split[0] switch
            {
                "/interact" => Interact(botClient, callbackQuery, split[1]),
                "/start" => Start(botClient, callbackQuery, split[1]),
                // TODO: "/stop" => Stop(botClient, callbackQuery),

                _ => null
            };

            async Task<Message> Interact(ITelegramBotClient botClient, CallbackQuery callbackQuery, string botName)
            {
                await botClient.SendChatActionAsync(callbackQuery.Message.Chat.Id, ChatAction.Typing);

                List<BotInfo> allBots = GetAllBotsByUserId(callbackQuery.Message.Chat.Id).Result;

                BotInfo botInfo = allBots.Find(b => b.Name == botName);

                if (botInfo is null)
                {
                    return await botClient.SendTextMessageAsync(chatId: callbackQuery.Message.Chat.Id,
                                                                text: "You have no bot with this name!");
                }

                var markups = new List<InlineKeyboardButton>();
                markups.Add(InlineKeyboardButton.WithCallbackData("Start", "/start " + botName));

                //TODO:
                //markups.Add(InlineKeyboardButton.WithCallbackData("Stop", "/stop " + botName));
                //markups.Add(InlineKeyboardButton.WithCallbackData("Remove", "/remove " + botName));
                //markups.Add(InlineKeyboardButton.WithCallbackData("Status", "/status " + botName));

                InlineKeyboardMarkup inlineKeyboard = new (markups.ToArray());

                await botClient.AnswerCallbackQueryAsync(
                    callbackQueryId: callbackQuery.Id,
                    null);

                return await botClient.SendTextMessageAsync(
                    protectContent: true,
                    chatId: callbackQuery.Message!.Chat.Id,
                    text: $"How do you want to interact with your bot {botName}?",
                    replyMarkup: inlineKeyboard);
            }

            async Task<Message> Start(ITelegramBotClient botClient, CallbackQuery callbackQuery, string botName)
            {
                await botClient.SendChatActionAsync(callbackQuery.Message.Chat.Id, ChatAction.Typing);

                List<BotInfo> allBots = GetAllBotsByUserId(callbackQuery.Message.Chat.Id).Result;

                BotInfo botInfo = allBots.Find(b => b.Name == botName);

                if (botInfo is null)
                {
                    return await botClient.SendTextMessageAsync(chatId: callbackQuery.Message.Chat.Id,
                                                                text: "You have no bot with this name!");
                }

                if (UserBotStart(botInfo))
                {
                    return await botClient.SendTextMessageAsync(chatId: callbackQuery.Message.Chat.Id,
                                                                text: $"Starting @{botInfo.TelegramUsername} success!");
                }
                else
                {
                    return await botClient.SendTextMessageAsync(chatId: callbackQuery.Message.Chat.Id,
                                                                text: $"Starting {botInfo.TelegramUsername} failed!");
                }
            }
        }

        public async Task<List<BotInfo>> GetAllBotsByUserId(long userId)
        {
            DAL.Entities.User currUser = _userService.GetUserByTelegramId(userId).Result;
            if (currUser == null) return new List<BotInfo>();

            return await _botService.GetAllUserBots(currUser.Id);
        }
    }
}
