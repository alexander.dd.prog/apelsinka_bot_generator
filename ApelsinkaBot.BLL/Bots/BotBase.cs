﻿using ApelsinkaBot.DAL.Entities;
using System;
using System.Threading;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Exceptions;
using Telegram.Bot.Polling;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;

namespace ApelsinkaBot.BLL.Bots
{
    public abstract class BotBase : IBot
    {
        public TelegramBotClient Client { get; set; }
        public BotInfo BotInfo { get; set; }
        protected CancellationTokenSource cts = new CancellationTokenSource();
        protected string UsageCaption = "Usage:" + Environment.NewLine
            //+ "/remove - remove keyboard" + Environment.NewLine
            ;

        public BotBase(BotInfo botInfo)
        {
            BotInfo = botInfo;
            Client = new TelegramBotClient(botInfo.Token);
        }

        public async Task<bool> StartAsync()
        {
            var receiverOptions = new ReceiverOptions()
            {
                AllowedUpdates = Array.Empty<UpdateType>(),
                ThrowPendingUpdates = true,
            };

            using (cts)
            {
                Client.StartReceiving(updateHandler: HandleUpdateAsync,
                               pollingErrorHandler: PollingErrorHandler,
                               receiverOptions: receiverOptions,
                               cancellationToken: cts.Token);

                return await Client.TestApiAsync(cts.Token);
            }
        }

        public void Stop()
        {
            cts.Cancel();
        }

        public virtual Task PollingErrorHandler(ITelegramBotClient botClient, Exception exception, CancellationToken cancellationToken)
        {
            var ErrorMessage = exception switch
            {
                ApiRequestException apiRequestException => $"Telegram API Error:\n[{apiRequestException.ErrorCode}]\n{apiRequestException.Message}",
                _ => exception.ToString()
            };

            Console.WriteLine(ErrorMessage);
            return Task.CompletedTask;
        }

        public virtual async Task HandleUpdateAsync(ITelegramBotClient botClient, Update update, CancellationToken cancellationToken)
        {
            var handler = update.Type switch
            {
                UpdateType.Message => BotOnMessageReceived(botClient, update.Message!),
                UpdateType.EditedMessage => BotOnMessageReceived(botClient, update.EditedMessage!),
                UpdateType.CallbackQuery => BotOnCallbackQueryReceived(botClient, update.CallbackQuery!),
                _ => UnknownUpdateHandlerAsync(botClient, update)
            };

            try
            {
                await handler;
            }
#pragma warning disable CA1031
            catch (Exception exception)
#pragma warning restore CA1031
            {
                await PollingErrorHandler(botClient, exception, cancellationToken);
            }
        }

        protected virtual async Task BotOnMessageReceived(ITelegramBotClient botClient, Message message)
        {
            Console.WriteLine($"Receive message type: {message.Type}");
            if (message.Text is not { } messageText)
                return;

            Message sentMessage = messageText.Split(' ')[0] switch
            {
                //"/remove" => await RemoveKeyboard(botClient, message),
                _ => await Usage(botClient, message)
            };

            Console.WriteLine($"The message was sent with id: {sentMessage.MessageId}");

            //static async Task<Message> RemoveKeyboard(ITelegramBotClient botClient, Message message)
            //{
            //    return await botClient.SendTextMessageAsync(chatId: message.Chat.Id,
            //                                                text: "Removing keyboard",
            //                                                replyMarkup: new ReplyKeyboardRemove());
            //}

            async Task<Message> Usage(ITelegramBotClient botClient, Message message)
            {
                return await botClient.SendTextMessageAsync(chatId: message.Chat.Id,
                                                            text: UsageCaption,
                                                            replyMarkup: new ReplyKeyboardRemove());
            }
        }

        protected virtual Task BotOnCallbackQueryReceived(ITelegramBotClient botClient, CallbackQuery callbackQuery)
        {
            return Task.CompletedTask;
        }

        private static Task UnknownUpdateHandlerAsync(ITelegramBotClient botClient, Update update)
        {
            Console.WriteLine($"Unknown update type: {update.Type}");
            return Task.CompletedTask;
        }
    }
}
