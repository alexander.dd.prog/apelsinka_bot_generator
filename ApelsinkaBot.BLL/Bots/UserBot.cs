﻿using ApelsinkaBot.BLL.Services.Contracts;
using ApelsinkaBot.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;

namespace ApelsinkaBot.BLL.Bots
{
    public class UserBot : BotBase
    {
        protected readonly ISurveyService _surveyService;
        protected readonly ISurveyPointService _surveyPointService;
        protected readonly ISurveyHistoryService _surveyHistoryService;

        public UserBot(BotInfo botInfo, ISurveyService surveyService, ISurveyPointService surveyPointService, ISurveyHistoryService surveyHistoryService) : base(botInfo)
        {
            _surveyService = surveyService;
            _surveyPointService = surveyPointService;
            _surveyHistoryService = surveyHistoryService;

            UsageCaption += "/surveys - surveys list" + Environment.NewLine;
        }

        protected override async Task BotOnMessageReceived(ITelegramBotClient botClient, Message message)
        {
            Console.WriteLine($"Receive in UserBot, message type: {message.Type}");
            if (message.Text is not { } messageText)
                return;

            Message sentMessage = messageText.Split(' ')[0] switch
            {
                "/surveys" => await ShowUserSurveys(botClient, message),
                _ => null
            };

            if (sentMessage is null)
            {
                await base.BotOnMessageReceived(botClient, message);
                return;
            }

            Console.WriteLine($"The message was sent with id: {sentMessage.MessageId}");

            async Task<Message> ShowUserSurveys(ITelegramBotClient botClient, Message message)
            {
                await botClient.SendChatActionAsync(message.Chat.Id, ChatAction.Typing);

                List<Survey> surveys = _surveyService.GetAllBotSurveys(BotInfo.Id).Result;

                if (surveys.Count == 0)
                {
                    return await botClient.SendTextMessageAsync(chatId: message.Chat.Id,
                                                                text: "It bot have no surveys :(\n you can configure it in the MainBot");
                }

                InlineKeyboardMarkup inlineKeyboard = new(
                    surveys.Select(survey => InlineKeyboardButton.WithCallbackData(survey.Name, "/start " + survey.Id))
                );

                return await botClient.SendTextMessageAsync(chatId: message.Chat.Id,
                                                            text: "Choose survey to start:",
                                                            replyMarkup: inlineKeyboard);
            }
        }

        protected override async Task BotOnCallbackQueryReceived(ITelegramBotClient botClient, CallbackQuery callbackQuery)
        {
            if (callbackQuery.Data is not { } dataText)
                return;

            string[] split = dataText.Split(' ');

            if (split.Length < 2)
            {
                await botClient.SendTextMessageAsync(chatId: callbackQuery.Message.Chat.Id,
                                            text: "Command is not defined");
                return;
            }

            await botClient.SendChatActionAsync(callbackQuery.Message.Chat.Id, ChatAction.Typing);

            var action = split[0] switch
            {
                "/start" => Start(botClient, callbackQuery, split[1]),
                "/operate" => OperateSurvey(botClient, callbackQuery, split[1]),
                _ => null
            };

            async Task<Message> Start(ITelegramBotClient botClient, CallbackQuery callbackQuery, string surveyId)
            {
                List<Survey> surveys = _surveyService.GetAllBotSurveys(BotInfo.Id).Result;

                Survey survey = surveys.Find(b => b.Id.ToString() == surveyId);

                if (survey is null)
                {
                    return await botClient.SendTextMessageAsync(chatId: callbackQuery.Message.Chat.Id,
                                                                text: "Survey with this name not found!");
                }

                return await OperateSurvey(botClient, callbackQuery, survey.FirstSurveyPointId.ToString());
            }

            async Task<Message> OperateSurvey(ITelegramBotClient botClient, CallbackQuery callbackQuery, string surveyPointIdStr)
            {
                int surveyPointId;
                if (!int.TryParse(surveyPointIdStr, out surveyPointId))
                {
                    return await botClient.SendTextMessageAsync(chatId: callbackQuery.Message.Chat.Id,
                                                                text: "Unable to parse command");
                }

                SurveyPoint currentSurveyPoint = _surveyPointService.GetBySingleId(surveyPointId).Result;

                List<SurveyPoint> children = _surveyPointService.GetNextAsync(currentSurveyPoint).Result;
                
                var markups = new List<InlineKeyboardButton>();

                if (currentSurveyPoint.Type == "q")
                {
                    foreach (SurveyPoint child in children)
                    {
                        markups.Add(InlineKeyboardButton.WithCallbackData(child.SurveyText, "/operate " + child.Id));
                    }

                    InlineKeyboardMarkup inlineKeyboard = new(markups.ToArray());

                    return await botClient.EditMessageTextAsync(
                        chatId: callbackQuery.Message!.Chat.Id,
                        messageId: callbackQuery.Message!.MessageId,
                        text: currentSurveyPoint.SurveyText,
                        replyMarkup: inlineKeyboard);
                }
                else // User's answer.
                {
                    await _surveyHistoryService.InsertAsync(new SurveyHistory()
                    {
                        Dt = DateTime.Now.ToUniversalTime(),
                        SurveyPointId = surveyPointId,
                        ChatId = callbackQuery.Message!.Chat.Id,
                    });

                    if (children.Count == 0)
                    {
                        return await botClient.EditMessageTextAsync(
                            chatId: callbackQuery.Message.Chat.Id,
                            messageId: callbackQuery.Message!.MessageId,
                            text: "Survey ended!");
                    }

                    return await OperateSurvey(botClient, callbackQuery, children[0].Id.ToString());
                }
            }
        }
    }
}
