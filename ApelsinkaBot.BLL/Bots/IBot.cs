﻿using ApelsinkaBot.DAL.Entities;
using System.Threading.Tasks;
using Telegram.Bot;

namespace ApelsinkaBot.BLL.Bots
{
    public interface IBot
    {
        TelegramBotClient Client { get; set; }
        public BotInfo BotInfo { get; set; }
        Task<bool> StartAsync();
        void Stop();
    }
}
