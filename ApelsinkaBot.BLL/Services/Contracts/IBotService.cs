﻿using ApelsinkaBot.DAL.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ApelsinkaBot.BLL.Services.Contracts
{
    public interface IBotService : IServiceBase<BotInfo>
    {
        Task<List<BotInfo>> GetAllUserBots(int userId);
    }
}
