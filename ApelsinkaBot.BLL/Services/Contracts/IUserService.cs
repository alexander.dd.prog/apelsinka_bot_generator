﻿using ApelsinkaBot.DAL.Entities;
using System.Threading.Tasks;

namespace ApelsinkaBot.BLL.Services.Contracts
{
    public interface IUserService : IServiceBase<User>
    {
        Task<User> GetUserByPhone(string phone);
        Task<User> GetUserByTelegramId(long telegramId);
    }
}
