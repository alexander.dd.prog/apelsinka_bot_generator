﻿using ApelsinkaBot.DAL.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ApelsinkaBot.BLL.Services.Contracts
{
    public interface ISurveyHistoryService : IServiceBase<SurveyHistory>
    {
        Task<SurveyHistory> GetLast();
    }
}
