﻿using ApelsinkaBot.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApelsinkaBot.BLL.Services.Contracts
{
    public interface ISurveyPointService : IServiceBase<SurveyPoint>
    {
        Task<List<SurveyPoint>> GetNextAsync(SurveyPoint current);
    }
}
