﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace ApelsinkaBot.BLL.Services.Contracts
{
    public interface IServiceBase<T> where T : class
    {
        Task<T> GetBySingleId(int id);
        Task<List<T>> GetByMultipleIds(IEnumerable<int> id);
        Task<List<T>> GetAll();
        Task InsertAsync(T entity);
        Task UpdateAsync(T entity);
        Task Delete(T entity);
    }
}
