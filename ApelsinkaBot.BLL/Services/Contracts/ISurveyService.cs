﻿using ApelsinkaBot.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApelsinkaBot.BLL.Services.Contracts
{
    public interface ISurveyService : IServiceBase<Survey>
    {
        Task<List<Survey>> GetAllBotSurveys(int bot_id);
    }
}
