﻿using ApelsinkaBot.BLL.Services.Contracts;
using ApelsinkaBot.DAL.Repositories.Contracts;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ApelsinkaBot.BLL.Services
{
    public abstract class ServiceBase<R, T> : IServiceBase<T> where R : IRepositoryWithId<T>, IBaseRepository<T>
                                                              where T : class
    {
        protected R _repository;
        protected ServiceBase(R repository)
        {
            _repository = repository;
        }

        public Task<T> GetBySingleId(int id)
        {
            return _repository.GetBySingleId(id);
        }

        public Task<List<T>> GetByMultipleIds(IEnumerable<int> id)
        {
            return _repository.GetByMultipleIds(id);
        }

        public virtual Task<List<T>> GetAll()
        {
            return _repository.GetAll();
        }

        public virtual Task Delete(T entity)
        {
            return _repository.Delete(entity);
        }

        public virtual async Task InsertAsync(T entity)
        {
            await _repository.Insert(entity);
            await _repository.SaveChanges();
        }

        public virtual async Task UpdateAsync(T entity)
        {
            await _repository.Update(entity);
            await _repository.SaveChanges();
        }
    }
}
