﻿using ApelsinkaBot.BLL.Services.Contracts;
using ApelsinkaBot.DAL.Entities;
using ApelsinkaBot.DAL.Repositories.Contracts;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ApelsinkaBot.BLL.Services
{
    public class SurveyPointService : ServiceBase<ISurveyPointRepository, SurveyPoint>, ISurveyPointService
    {
        private readonly ISurveyConnectionRepository _surveyConnectionRepository;
        public SurveyPointService(ISurveyPointRepository repository, ISurveyConnectionRepository surveyConnectionRepository) : base(repository)
        {
            _surveyConnectionRepository = surveyConnectionRepository;
        }

        public async Task<List<SurveyPoint>> GetNextAsync(SurveyPoint current)
        {
            List<SurveyPoint> output = new List<SurveyPoint>();

            List<int> surveyConnectionIDs = await _surveyConnectionRepository.GetByParentID(current.Id);

            foreach (var id in surveyConnectionIDs)
            {
                SurveyPoint sp = await _repository.GetBySingleId(id);
                    
                if (sp != null)
                {
                    output.Add(sp);
                }
            }

            return output;
        }
    }
}
