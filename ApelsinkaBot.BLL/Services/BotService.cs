﻿using ApelsinkaBot.BLL.Services.Contracts;
using ApelsinkaBot.DAL.Entities;
using ApelsinkaBot.DAL.Repositories.Contracts;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ApelsinkaBot.BLL.Services
{
    public class BotService : ServiceBase<IBotRepository, BotInfo>, IBotService
    {
        public BotService(IBotRepository repository) : base(repository)
        {
        }

        public async Task<List<BotInfo>> GetAllUserBots(int userId)
        {
            return await _repository.GetAllUserBots(userId);
        }
    }
}
