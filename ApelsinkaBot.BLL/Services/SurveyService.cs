﻿using ApelsinkaBot.BLL.Services.Contracts;
using ApelsinkaBot.DAL.Entities;
using ApelsinkaBot.DAL.Repositories.Contracts;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ApelsinkaBot.BLL.Services
{
    public class SurveyService : ServiceBase<ISurveyRepository, Survey>, ISurveyService
    {
        public SurveyService(ISurveyRepository repository) : base(repository)
        {

        }

        public async Task<List<Survey>> GetAllBotSurveys(int bot_id)
        {
            return await _repository.GetAllBotSurveys(bot_id);
        }
    }
}
