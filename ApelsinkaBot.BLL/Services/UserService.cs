﻿using ApelsinkaBot.BLL.Services.Contracts;
using ApelsinkaBot.DAL.Entities;
using ApelsinkaBot.DAL.Repositories.Contracts;
using System.Threading.Tasks;

namespace ApelsinkaBot.BLL.Services
{
    public class UserService : ServiceBase<IUserRepository, User>, IUserService
    {
        public UserService(IUserRepository repository) : base(repository)
        {
        }

        public async Task<User> GetUserByPhone(string phone)
        {
            return await _repository.GetUserByPhone(phone);
        }
        public async Task<User> GetUserByTelegramId(long telegramId)
        {
            return await _repository.GetUserByTelegramId(telegramId);
        }
    }
}
