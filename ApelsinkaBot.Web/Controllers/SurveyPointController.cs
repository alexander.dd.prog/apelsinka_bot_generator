﻿using ApelsinkaBot.BLL.Services.Contracts;
using ApelsinkaBot.DAL.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ApelsinkaBot.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SurveyPointController : ControllerBase
    {
        private readonly ISurveyPointService _surveyPointService;

        public SurveyPointController(ISurveyPointService surveyPointService)
        {
            _surveyPointService = surveyPointService;
        }

        [HttpGet("{id:int}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetNext([FromRoute] int id)
        {
            SurveyPoint surveyPoint = await _surveyPointService.GetBySingleId(id);

            if (surveyPoint is null)
            {
                return NotFound("Узла опроса с данным Id не существует.");
            }

            List<SurveyPoint> surveyPoints = await _surveyPointService.GetNextAsync(surveyPoint);

            return Ok(surveyPoints);
        }
    }
}
