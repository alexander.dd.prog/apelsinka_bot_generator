﻿using ApelsinkaBot.BLL.Services.Contracts;
using ApelsinkaBot.DAL.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ApelsinkaBot.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SurveyController : ControllerBase
    {
        private readonly ISurveyService _surveyService;

        public SurveyController(ISurveyService surveyService)
        {
            _surveyService = surveyService;
        }

        [HttpGet("{id:int}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetSurvey([FromRoute] int id)
        {
            Survey survey = await _surveyService.GetBySingleId(id);

            if (survey is null)
            {
                return NotFound("Опроса с данным Id не существует.");
            }

            return Ok(survey);
        }

        [HttpGet("by_bot_id/{bot_id:int}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetAllSurveysByBotId([FromRoute] int bot_id)
        {
            List<Survey> surveys = await _surveyService.GetAllBotSurveys(bot_id);

            if (surveys.Count == 0)
            {
                return NotFound("У бота отсутствуют опросы");
            }
            else
            {
                return Ok(surveys);
            }
        }
    }
}
