﻿using ApelsinkaBot.BLL.Services.Contracts;
using ApelsinkaBot.DAL.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace ApelsinkaBot.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _service;

        public UserController(IUserService service)
        {
            _service = service;
        }

        [HttpGet("{id:int}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetUser([FromRoute] int id)
        {
            User user = await _service.GetBySingleId(id);

            if (user is null)
            {
                return NotFound("Пользователя с данным Id не существует.");
            }

            return Ok(user);
        }

        [HttpGet("{phone}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetUserByPhone([FromRoute] string phone)
        {
            User user = await _service.GetUserByPhone(phone);

            if (user is null)
            {
                return NotFound("Пользователя с данным телефоном не существует.");
            }

            return Ok(user);
        }

    }
}
