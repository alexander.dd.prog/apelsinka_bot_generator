﻿using ApelsinkaBot.BLL.Services;
using ApelsinkaBot.BLL.Services.Contracts;
using ApelsinkaBot.DAL.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ApelsinkaBot.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BotController : ControllerBase
    {
        private readonly IBotService _service;

        public BotController(IBotService service)
        {
            _service = service;
        }

        [HttpGet("{id:int}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetBot([FromRoute] int id)
        {
            BotInfo bot = await _service.GetBySingleId(id);

            if (bot is null)
            {
                return NotFound("Бота с данным Id не существует.");
            }

            return Ok(bot);
        }

        [HttpGet("user/{id:int}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetAllUserBots([FromRoute] int id)
        {
            List<BotInfo> bots = await _service.GetAllUserBots(id);

            if (bots.Count == 0)
            {
                return NotFound("У пользователя нет ботов");
            }
            else
            {
                return Ok(bots);
            }
        }
    }
}
